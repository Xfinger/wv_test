bundle install
cd app/test
ruby make_db.rb creates the db
ruby datatest.rb builds the db from query


_____________  
  
Try the jsonPath online evaluator  
go here  
http://ashphy.com/JSONPathOnlineEvaluator/  
  
  
put this in the JsonPath syntax  
$.Envelope.Body.validateResponse.return.address.codes.messages[5][value]  
  
try removing fields from the end of the above and you can move up in the hash  

  
put this in the json field  
{
    "Envelope": {
        "xmlns:S": "http://schemas.xmlsoap.org/soap/envelope/",
        "Body": {
            "validateResponse": {
                "xmlns:ns2": "http://ws.worldview.globaldata.com/",
                "return": {
                    "address": {
                        "addressLine1": "420 9TH AVE",
                        "addressLine10": null,
                        "addressLine2": "NEW YORK NY 10001-1614",
                        "addressLine3": null,
                        "addressLine4": null,
                        "addressLine5": null,
                        "addressLine6": null,
                        "addressLine7": null,
                        "addressLine8": null,
                        "addressLine9": null,
                        "building": null,
                        "cebucoCode": null,
                        "census": null,
                        "codes": {
                            "adaptation": "30",
                            "detailCode": "WS-5532715.2015.7.4.15.35.54.966",
                            "detail_list": "Return_VelocityExceeded = N | err_code = 0 | mc = 460 | #text = N | tid = UE11Q364K10761471157 | err_desc =  | Return_IDCheck = 0 | mc_desc = state;last;first_i;mmddyyyy | ||CountryName = 5United States | Value = USA | GeoAccuracy = 5A2 | _L = 5Geocode|Country | AdministrativeArea = NY | Building = 420 | #text = USA | Thoroughfare = 9th | Status = 5 | PostalCode = 10001-1614 | Locality = New York | Address2 = New York Ny 10001-1614 | Address1 = 420 9th Ave | AVC = U00-U00-P0-000 | Latitude = 540.699039 | Longitude = 5-73.895710 | GeoDistance = 557419.1 | ISO3166-N = 5840 | ISO3166-2 = 5US | ISO3166-3 = 5USA | ||",
                            "messages": [
                                {
                                    "code": "A3",
                                    "value": "Major changes made to the input address."
                                },
                                {
                                    "code": "EP",
                                    "value": "(Enhanced Postal Code) Postal Code a partial match or enhanced related to input data"
                                },
                                {
                                    "code": "R1",
                                    "value": "The record has been verified by the provider as correct."
                                },
                                {
                                    "code": "AreaType-MUR",
                                    "value": "AreaType-NonResidential or Mixed Use"
                                },
                                {
                                    "code": "0",
                                    "value": "No significant changes"
                                },
                                {
                                    "code": "detail_code",
                                    "value": "WS-5532715.2015.7.4.15.35.54.966"
                                },
                                {
                                    "code": "CRN:Reference Number for demo",
                                    "value": "CRN:12345 this is the reference number:demo:demo"
                                }
                            ],
                            "options": "IdentityVerify;GeoCoding;OutputExtendedDetail;RemoveDuplicateValues",
                            "reliability": "10"
                        },
                        "congressionalDistrict": "10",
                        "countryCode": "US",
                        "countryISO3": "USA",
                        "countryName": "United States of America",
                        "countryNumber": "840",
                        "county": "NEW YORK",
                        "countyCode": "36061",
                        "department": null,
                        "district": null,
                        "dpvCoded": "false",
                        "dpvFootnotes": null,
                        "dpvIndicator": " ",
                        "dpvIsCMRA": "false",
                        "dpvIsNoStat": "false",
                        "dpvIsPBSA": "false",
                        "dpvIsVacant": "false",
                        "eggCode": null,
                        "formalCountryName": "United States of America",
                        "hausCode": null,
                        "houseNumber": "420",
                        "houseNumberAddition": "AVE",
                        "lacsIndicator": null,
                        "latitude": "40.699039",
                        "locality": "NEW YORK",
                        "longitude": "-73.895710",
                        "lotNumber": null,
                        "municipalityCode": null,
                        "name": null,
                        "organization": null,
                        "postBox": null,
                        "postIndicator": null,
                        "postTown": null,
                        "postalCode": "10001-1614",
                        "preIndicator": null,
                        "premise": null,
                        "province": "NY",
                        "provinceCode": null,
                        "subDistrict": null,
                        "thoroughfare": "9TH",
                        "timezone": null,
                        "urbanArea": null,
                        "urbanization": null
                    },
                    "codes": {
                        "adaptation": "30",
                        "detailCode": "WS-5532715.2015.7.4.15.35.54.966",
                        "detail_list": null,
                        "options": "IdentityVerify;GeoCoding;OutputExtendedDetail;RemoveDuplicateValues",
                        "reliability": "30"
                    },
                    "email": {
                        "addressee": "michael.jackson",
                        "codes": {
                            "adaptation": "30",
                            "detailCode": "WS-5532715.2015.7.4.15.35.54.966",
                            "detail_list": "addressee: michael.jackson|displayName: |domainName: gmail.com|fullEmailAddress: michael.jackson@gmail.com|status: Valid syntax of emailVALIDSYNTAX1|status: Valid domainVALID2|topLevelDomain: com|severity: 1||",
                            "messages": [
                                {
                                    "code": "VALIDSYNTAX",
                                    "value": "Valid syntax of email"
                                },
                                {
                                    "code": "VALID",
                                    "value": "Valid domain"
                                }
                            ],
                            "options": "IdentityVerify;GeoCoding;OutputExtendedDetail;RemoveDuplicateValues",
                            "reliability": "10"
                        },
                        "countryCode": null,
                        "display_name": null,
                        "domain": "gmail.com",
                        "full_email_address": "michael.jackson@gmail.com",
                        "top_level": "com"
                    },
                    "identity": {
                        "businessid": null,
                        "codes": {
                            "adaptation": "30",
                            "detailCode": "WS-5532715.2015.7.4.15.35.54.966",
                            "detail_list": null,
                            "messages": [
                                {
                                    "code": "1AU",
                                    "value": "Full match was made with element Home_city"
                                },
                                {
                                    "code": "368",
                                    "value": "First/Last/Home_Address/Home_city/Home_State"
                                },
                                {
                                    "code": "1AU",
                                    "value": "Full match was made with element Home_Address"
                                },
                                {
                                    "code": "1AU",
                                    "value": "Full match was made with element Last"
                                },
                                {
                                    "code": "1AU",
                                    "value": "Full match was made with element Home_State"
                                },
                                {
                                    "code": "1AU",
                                    "value": "Full match was made with element First"
                                }
                            ],
                            "options": "IdentityVerify;GeoCoding;OutputExtendedDetail;RemoveDuplicateValues",
                            "reliability": "20"
                        },
                        "completename": "Justin Williams",
                        "contact_type": "Person",
                        "countryCode": null,
                        "dob": "08/04/1988",
                        "formofaddress": null,
                        "function": null,
                        "gender": "Male",
                        "givenfullname": "Justin",
                        "givennameinitials": null,
                        "indicator": null,
                        "name_qualified": null,
                        "nationalid": null,
                        "nationality": null,
                        "organization_name": null,
                        "passport": null,
                        "qualification_int_first": null,
                        "qualification_int_second": null,
                        "qualification_suceeding": null,
                        "qualificationpreceding": null,
                        "surname_first": "Williams",
                        "surname_prefix_first": null,
                        "surname_prefix_second": null
                    },
                    "phone": {
                        "area_code": "1919",
                        "calling_code": "1",
                        "calling_from": null,
                        "codes": {
                            "adaptation": "30",
                            "detailCode": "WS-5532715.2015.7.4.15.35.54.966",
                            "detail_list": "areaCode: 1919|countryCallingCode: 1|formattedOutOfCountryCalling: 1 (919) 601-1428|formattedPhoneNumber: +19196011428|internationalCallPrefix: |lineType: FIXED_LINE_OR_MOBILE|status: Green100Phone Number is valid101Phone Number matches country code in input1|subscriberNumber: 6011428|trunkPrefix: ||",
                            "messages": [
                                {
                                    "code": "101",
                                    "value": "Phone Number matches country code in input"
                                },
                                {
                                    "code": "100",
                                    "value": "Phone Number is valid"
                                }
                            ],
                            "options": "IdentityVerify;GeoCoding;OutputExtendedDetail;RemoveDuplicateValues",
                            "reliability": "10"
                        },
                        "countryCode": null,
                        "formatted_international": "+19196011428",
                        "international_prefix": null,
                        "line_type": "FIXED_LINE_OR_MOBILE",
                        "name": null,
                        "number": "6011428",
                        "phone_number": "1 (919) 601-1428",
                        "timezone": null,
                        "trunk": null
                    }
                }
            }
        }
    }
}