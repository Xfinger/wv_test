 

require 'rubygems'
require 'json'
require 'sqlite3'
require 'active_record'
require 'sqlite3'
require 'jsonpath' 

db_options = {adapter: 'sqlite3', database: 'chad.db'}
DB = ActiveRecord::Base.establish_connection(db_options)  #set up db connection

	

class Issue < ActiveRecord::Base #your class needs to inherit from ActiveRecord::Base - this is the Issues table in the DB
 
end

class Testing

 def initialize #not necessary for this example but may be useful in main app
   
 end

 def initial_build 
  
  @response = ({"Envelope"=>{"xmlns:S"=>"http://schemas.xmlsoap.org/soap/envelope/", "Body"=>{"validateResponse"=>{"xmlns:ns2"=>"http://ws.worldview.globaldata.com/", "return"=>{"address"=>{"addressLine1"=>nil, "addressLine10"=>nil, "addressLine2"=>nil, "addressLine3"=>nil, "addressLine4"=>nil, "addressLine5"=>nil, "addressLine6"=>nil, "addressLine7"=>nil, "addressLine8"=>nil, "addressLine9"=>nil, "building"=>nil, "cebucoCode"=>nil, "census"=>nil, "codes"=>{"adaptation"=>"30", "detailCode"=>"WS-5402402.2015.6.23.10.37.26.42", "detail_list"=>nil, "messages"=>[{"code"=>"CRN:Reference Number for local", "value"=>"CRN::local:local:WS-1771927.2015.6.23.10.36.17.23"}, {"code"=>"U", "value"=>"Unverified. Unable to verify. The output fields will contain the input data"}, {"code"=>"WV", "value"=>"Address was not verified.  Original input returned."}, {"code"=>"P0", "value"=>"Post-Processed Verification Match Level. None"}, {"code"=>"PC0", "value"=>"Postcode Status. PostalCodePrimary empty"}, {"code"=>"detail_code", "value"=>"WS-5402402.2015.6.23.10.37.26.42"}], "options"=>"RemoveDuplicateValues", "reliability"=>"30"}, "congressionalDistrict"=>nil, "countryCode"=>"US", "countryISO3"=>"USA", "countryName"=>"United States of America", "countryNumber"=>"840", "county"=>nil, "countyCode"=>nil, "department"=>nil, "district"=>nil, "dpvCoded"=>nil, "dpvFootnotes"=>nil, "dpvIndicator"=>nil, "dpvIsCMRA"=>nil, "dpvIsNoStat"=>nil, "dpvIsPBSA"=>nil, "dpvIsVacant"=>nil, "eggCode"=>nil, "formalCountryName"=>"United States of America", "hausCode"=>nil, "houseNumber"=>nil, "houseNumberAddition"=>nil, "lacsIndicator"=>nil, "latitude"=>nil, "locality"=>nil, "longitude"=>nil, "lotNumber"=>nil, "municipalityCode"=>nil, "name"=>nil, "organization"=>nil, "postBox"=>nil, "postIndicator"=>nil, "postTown"=>nil, "postalCode"=>nil, "preIndicator"=>nil, "premise"=>nil, "province"=>nil, "provinceCode"=>nil, "subDistrict"=>nil, "thoroughfare"=>nil, "timezone"=>nil, "urbanArea"=>nil, "urbanization"=>nil}, "codes"=>{"adaptation"=>"30", "detailCode"=>"WS-5402402.2015.6.23.10.37.26.42", "detail_list"=>nil, "options"=>"RemoveDuplicateValues", "reliability"=>"30"}, "email"=>{"addressee"=>nil, "codes"=>{"adaptation"=>"30", "detailCode"=>"WS-5402402.2015.6.23.10.37.26.42", "detail_list"=>nil, "options"=>"RemoveDuplicateValues", "reliability"=>"30"}, "countryCode"=>nil, "display_name"=>"?", "domain"=>nil, "full_email_address"=>nil, "top_level"=>nil}, "identity"=>{"businessid"=>nil, "codes"=>{"adaptation"=>"30", "detailCode"=>"WS-5402402.2015.6.23.10.37.26.42", "detail_list"=>nil, "messages"=>{"code"=>"WV", "value"=>"Identity was not verified.  Original input returned."}, "options"=>"RemoveDuplicateValues", "reliability"=>"30"}, "completename"=>nil, "contact_type"=>nil, "countryCode"=>"US", "dob"=>nil, "formofaddress"=>nil, "function"=>nil, "gender"=>nil, "givenfullname"=>nil, "givennameinitials"=>nil, "indicator"=>nil, "name_qualified"=>nil, "nationalid"=>nil, "nationality"=>nil, "organization_name"=>nil, "passport"=>nil, "qualification_int_first"=>nil, "qualification_int_second"=>nil, "qualification_suceeding"=>nil, "qualificationpreceding"=>nil, "surname_first"=>nil, "surname_prefix_first"=>nil, "surname_prefix_second"=>nil}, "phone"=>{"area_code"=>nil, "calling_code"=>"US", "calling_from"=>"US", "codes"=>{"adaptation"=>"30", "detailCode"=>"WS-5402402.2015.6.23.10.37.26.42", "detail_list"=>nil, "options"=>"RemoveDuplicateValues", "reliability"=>"30"}, "countryCode"=>nil, "formatted_international"=>nil, "international_prefix"=>nil, "line_type"=>nil, "name"=>nil, "number"=>nil, "phone_number"=>nil, "timezone"=>nil, "trunk"=>nil}}}}}})
  @detail_code = @response["Envelope"]["Body"]["validateResponse"]["return"]["address"]["codes"]["detailCode"]
  @options = @response["Envelope"]["Body"]["validateResponse"]["return"]["address"]["codes"]["options"]
  @crn_value = @response["Envelope"]["Body"]["validateResponse"]["return"]["address"]["codes"]["messages"][0]["value"]

  #below just prints to screen so you can see all of the keys/sub keys 
  puts '________________________________'
  puts 'hash keys:'
  puts @response["Envelope"]["Body"]["validateResponse"]["return"].keys
  puts '________________________________'
  puts 'address keys:'
  puts @response["Envelope"]["Body"]["validateResponse"]["return"]["address"].keys
  puts '________________________________'
  puts 'codes keys:'
  puts @response["Envelope"]["Body"]["validateResponse"]["return"]["codes"].keys
  puts '________________________________'
  puts 'email keys:'
  puts @response["Envelope"]["Body"]["validateResponse"]["return"]["email"].keys
  puts '________________________________'
  puts 'identity keys:'
  puts @response["Envelope"]["Body"]["validateResponse"]["return"]["identity"].keys
  puts '________________________________'
  puts 'phone keys:'
  puts @response["Envelope"]["Body"]["validateResponse"]["return"]["phone"].keys
  puts '________________________________'
  #######################

  @code = Issue.create(:detailCode => @detail_code, :dc_options => @options, :crn_value => @crn_value) #create new row in database & insert values   
  @code.save

   end
  
  def show_results #this returns detailCode, options & crn_value using jsonpath
      json = ({"Envelope"=>{"xmlns:S"=>"http://schemas.xmlsoap.org/soap/envelope/", "Body"=>{"validateResponse"=>{"xmlns:ns2"=>"http://ws.worldview.globaldata.com/", "return"=>{"address"=>{"addressLine1"=>nil, "addressLine10"=>nil, "addressLine2"=>nil, "addressLine3"=>nil, "addressLine4"=>nil, "addressLine5"=>nil, "addressLine6"=>nil, "addressLine7"=>nil, "addressLine8"=>nil, "addressLine9"=>nil, "building"=>nil, "cebucoCode"=>nil, "census"=>nil, "codes"=>{"adaptation"=>"30", "detailCode"=>"WS-5402402.2015.6.23.10.37.26.42", "detail_list"=>nil, "messages"=>[{"code"=>"CRN:Reference Number for local", "value"=>"CRN::local:local:WS-1771927.2015.6.23.10.36.17.23"}, {"code"=>"U", "value"=>"Unverified. Unable to verify. The output fields will contain the input data"}, {"code"=>"WV", "value"=>"Address was not verified.  Original input returned."}, {"code"=>"P0", "value"=>"Post-Processed Verification Match Level. None"}, {"code"=>"PC0", "value"=>"Postcode Status. PostalCodePrimary empty"}, {"code"=>"detail_code", "value"=>"WS-5402402.2015.6.23.10.37.26.42"}], "options"=>"RemoveDuplicateValues", "reliability"=>"30"}, "congressionalDistrict"=>nil, "countryCode"=>"US", "countryISO3"=>"USA", "countryName"=>"United States of America", "countryNumber"=>"840", "county"=>nil, "countyCode"=>nil, "department"=>nil, "district"=>nil, "dpvCoded"=>nil, "dpvFootnotes"=>nil, "dpvIndicator"=>nil, "dpvIsCMRA"=>nil, "dpvIsNoStat"=>nil, "dpvIsPBSA"=>nil, "dpvIsVacant"=>nil, "eggCode"=>nil, "formalCountryName"=>"United States of America", "hausCode"=>nil, "houseNumber"=>nil, "houseNumberAddition"=>nil, "lacsIndicator"=>nil, "latitude"=>nil, "locality"=>nil, "longitude"=>nil, "lotNumber"=>nil, "municipalityCode"=>nil, "name"=>nil, "organization"=>nil, "postBox"=>nil, "postIndicator"=>nil, "postTown"=>nil, "postalCode"=>nil, "preIndicator"=>nil, "premise"=>nil, "province"=>nil, "provinceCode"=>nil, "subDistrict"=>nil, "thoroughfare"=>nil, "timezone"=>nil, "urbanArea"=>nil, "urbanization"=>nil}, "codes"=>{"adaptation"=>"30", "detailCode"=>"WS-5402402.2015.6.23.10.37.26.42", "detail_list"=>nil, "options"=>"RemoveDuplicateValues", "reliability"=>"30"}, "email"=>{"addressee"=>nil, "codes"=>{"adaptation"=>"30", "detailCode"=>"WS-5402402.2015.6.23.10.37.26.42", "detail_list"=>nil, "options"=>"RemoveDuplicateValues", "reliability"=>"30"}, "countryCode"=>nil, "display_name"=>"?", "domain"=>nil, "full_email_address"=>nil, "top_level"=>nil}, "identity"=>{"businessid"=>nil, "codes"=>{"adaptation"=>"30", "detailCode"=>"WS-5402402.2015.6.23.10.37.26.42", "detail_list"=>nil, "messages"=>{"code"=>"WV", "value"=>"Identity was not verified.  Original input returned."}, "options"=>"RemoveDuplicateValues", "reliability"=>"30"}, "completename"=>nil, "contact_type"=>nil, "countryCode"=>"US", "dob"=>nil, "formofaddress"=>nil, "function"=>nil, "gender"=>nil, "givenfullname"=>nil, "givennameinitials"=>nil, "indicator"=>nil, "name_qualified"=>nil, "nationalid"=>nil, "nationality"=>nil, "organization_name"=>nil, "passport"=>nil, "qualification_int_first"=>nil, "qualification_int_second"=>nil, "qualification_suceeding"=>nil, "qualificationpreceding"=>nil, "surname_first"=>nil, "surname_prefix_first"=>nil, "surname_prefix_second"=>nil}, "phone"=>{"area_code"=>nil, "calling_code"=>"US", "calling_from"=>"US", "codes"=>{"adaptation"=>"30", "detailCode"=>"WS-5402402.2015.6.23.10.37.26.42", "detail_list"=>nil, "options"=>"RemoveDuplicateValues", "reliability"=>"30"}, "countryCode"=>nil, "formatted_international"=>nil, "international_prefix"=>nil, "line_type"=>nil, "name"=>nil, "number"=>nil, "phone_number"=>nil, "timezone"=>nil, "trunk"=>nil}}}}}})
      d_code = JsonPath.new("$..Envelope.Body.validateResponse.return.address.codes.detailCode")
      opt = JsonPath.new("$..Envelope.Body.validateResponse.return.address.codes.options")
      crn = JsonPath.new("$..Envelope.Body.validateResponse.return.address.codes.messages[0].value")
      puts d_code.on(json)
      puts opt.on(json)
      puts crn.on(json)
      puts '___________________________________________'
      get_fromDB
  end
  def get_fromDB #get data back out of the db
    issues = Issue.all
    issues.each_with_index do |x,index|
      puts index
      puts x.detailCode
      puts x.dc_options
      puts x.crn_value
      puts '___________________________________________' 
    end

  end
 
   
end
 Testing.new.show_results
  